//
//  TestError.swift
//  NetworkResponseTests
//
//  Created by Sergio Andres Rodriguez Castillo on 18/01/24.
//

import Foundation

struct TestError: LocalizedError {
    let message: String
    
    var errorDescription: String? { message }
}
